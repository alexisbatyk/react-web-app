import React from "react"
import ReactDOM from "react-dom"
import { act } from "react-dom/test-utils"
import { expect } from "chai"
var jsdom = require("mocha-jsdom")

import CreateTaskComponent from "../src/components/create-task/create-task"

jsdom({
  url: "http://localhost:3000/",
  skipWindowCheck: true,
})

let rootContainer

beforeEach(() => {
  rootContainer = document.createElement("div")
  document.body.appendChild(rootContainer)
})

afterEach(() => {
  document.body.removeChild(rootContainer)
  rootContainer = null
})

describe("Create task component testing", () => {
  it("Render component", () => {
    act(() => {
      ReactDOM.render(<CreateTaskComponent />, rootContainer)
    })

    const createOne = rootContainer.querySelector("p.createOne")
    expect(createOne).to.exist

    const input = rootContainer.querySelector("input")
    expect(input.name).to.equal("title")
    expect(input.type).to.equal("text")
    expect(input.placeholder).to.equal("Title")
    expect(input.value).to.equal("")
  })

  it("Render component with value", () => {
    const title = "Create testing cases"
    act(() => {
      ReactDOM.render(<CreateTaskComponent title={title} />, rootContainer)
    })

    const createOne = rootContainer.querySelector("p.createOne")
    expect(createOne).to.exist

    const input = rootContainer.querySelector("input")
    expect(input.name).to.equal("title")
    expect(input.type).to.equal("text")
    expect(input.placeholder).to.equal("Title")
    expect(input.value).to.equal(title)

    const sendButton = rootContainer.querySelector("button.sendNewTask")
    expect(sendButton).to.exist
  })
})

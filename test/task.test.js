import React from "react"
import ReactDOM from "react-dom"
import { act } from "react-dom/test-utils"
import { expect } from "chai"
var jsdom = require("mocha-jsdom")

import TaskComponent from "../src/components/task-list/task"

jsdom({
  url: "http://localhost:3000/",
  skipWindowCheck: true,
})

let rootContainer

beforeEach(() => {
  rootContainer = document.createElement("div")
  document.body.appendChild(rootContainer)
})

afterEach(() => {
  document.body.removeChild(rootContainer)
  rootContainer = null
})

describe("Task Component Testing", () => {
  it("Renders Task", () => {
    const task = { title: "go to grocery", _id: "1234" }

    act(() => {
      ReactDOM.render(
        <TaskComponent task={task} isBeingUpdated={false} />,
        rootContainer
      )
    })

    const h3 = rootContainer.querySelector("h3")
    expect(h3.textContent).to.equal(task.title)

    const icon = rootContainer.querySelector("div.editIcon")
    expect(icon).to.exist

    const deleteIcon = rootContainer.querySelector("button.taskDeleteButton")
    expect(deleteIcon).to.exist
  })
})

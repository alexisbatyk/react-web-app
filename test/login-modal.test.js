import React from "react"
import ReactDOM from "react-dom"
import { act } from "react-dom/test-utils"
import { expect } from "chai"
var jsdom = require("mocha-jsdom")

import LoginModal from "../src/components/login/login-modal"

jsdom({
  url: "http://localhost:3000/",
  skipWindowCheck: true,
})

let rootContainer

beforeEach(() => {
  rootContainer = document.createElement("div")
  document.body.appendChild(rootContainer)
})

afterEach(() => {
  document.body.removeChild(rootContainer)
  rootContainer = null
})

describe("Login Component Testing", () => {
  it("Renders Login", () => {
    act(() => {
      ReactDOM.render(<LoginModal />, rootContainer)
    })
    const h3 = rootContainer.querySelector("h3")
    expect(h3.textContent).to.equal("Hi!")

    const p = rootContainer.querySelector("p")
    expect(p.textContent).to.equal("Forgot your account?")

    const button = rootContainer.querySelector("button")
    expect(button.className).to.equal("submit")
    expect(button).to.exist

    const input = rootContainer.querySelector("input")
    expect(input.className).to.equal("input")
    expect(input.placeholder).to.equal("User")
  })

  it("Renders Login with value", () => {
    const user = "alexis"
    act(() => {
      ReactDOM.render(<LoginModal user={user} />, rootContainer)
    })

    const input = rootContainer.querySelector("input")
    expect(input.value).to.equal(user)
  })
})

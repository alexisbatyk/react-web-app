import React, { useEffect } from "react"
import { connect } from "react-redux"

import TaskList from "./components/task-list/task-list"
import CreateTask from "./containers/create-task"
import SearchContainer from "./containers/search"
import LoginContainer from "./containers/login"
import CreateTaskButton from "./containers/create-task-button"

import { getList, createTask, deleteTask, editTask } from "./services/task"
import { login } from "./services/login"

import {
  publishList,
  setEditingTask,
  reloadTaskData,
  userHasLoggedInAction,
} from "./state-management/actions"

import styles from "./App.module.css"
import { default_task } from "./state-management/type_constants"
import { BiTask } from "react-icons/bi"

const monthNames = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
]

function App({
  publishList,
  clearEditing,
  tasks,
  filteredList,
  filterText,
  updatedTaskData,
  userHasLoggedIn,
  setUserHasLogged,
}) {
  const newTask = async (
    title = "default title",
    description = "default description"
  ) => {
    const response = await createTask(title, description)
    loadData()
  }

  const deleteOneTask = async (id = "") => {
    const response = await deleteTask(id)
    // should check if it was ok or not
    loadData()
  }

  const updateTask = async task => {
    const response = await editTask(task._id, task.title, task.description)

    clearEditing()
    updatedTaskData(response)
  }

  const loadData = async () => {
    const data = await getList()
    publishList(data)
  }

  useEffect(() => {
    if (userHasLoggedIn) {
      loadData()
    }
  }, [userHasLoggedIn])

  const date = new Date()

  return (
    <div className={styles.container}>
      <div className={styles.mainTitle}>
        <h1>{`${monthNames[date.getMonth()]} ${date.getDate()}`}</h1>
        <BiTask size={40} />
      </div>

      <SearchContainer />
      <TaskList
        tasks={filterText === "" ? tasks : filteredList}
        deleteTask={deleteOneTask}
        updateTask={updateTask}
        isFilterApplied={filterText && filterText !== ""}
      />
      <CreateTaskButton />
      <CreateTask submit={newTask} />
      <LoginContainer
        login={login}
        showModal={!userHasLoggedIn}
        userHasLoggedIn={setUserHasLogged}
      />
    </div>
  )
}

const mapStateToProps = state => {
  const { tasks, filteredList, filterText, userHasLoggedIn } = state
  return { tasks, filteredList, filterText, userHasLoggedIn }
}

const mapDispatchToProps = dispatch => {
  return {
    publishList: data => dispatch(publishList(data)),
    clearEditing: () => dispatch(setEditingTask(default_task)),
    updatedTaskData: task => dispatch(reloadTaskData(task)),
    setUserHasLogged: () => dispatch(userHasLoggedInAction()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)

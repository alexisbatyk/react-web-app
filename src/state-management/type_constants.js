export const EDIT_TASK = "EDIT_TASK"
export const LOAD_LIST = "LOAD_LIST"
export const RELOAD_TASK_DATA = "RELOAD_TASK_DATA"
export const CHANGE_FILTER = "CHANGE_FILTER"
export const USER_HAS_LOGGED_IN = "USER_HAS_LOGGED_IN"
export const START_SEARCH = "START_SEARCH"
export const END_SEARCH = "END_SEARCH"
export const OPEN_CREATE = "OPEN_CREATE"
export const CLOSE_CREATE = "CLOSE_CREATE"
export const UPDATE_CREATE_TASK_DATA = "UPDATE_CREATE_TASK_DATA"

export const default_task = { _id: "", title: "", description: "" }

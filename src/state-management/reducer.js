import {
  EDIT_TASK,
  LOAD_LIST,
  RELOAD_TASK_DATA,
  CHANGE_FILTER,
  USER_HAS_LOGGED_IN,
  START_SEARCH,
  END_SEARCH,
  OPEN_CREATE,
  CLOSE_CREATE,
} from "./type_constants"

const defaultState = {
  editingTask: { _id: "", title: "", description: "" },
  tasks: [],
  filteredList: [],
  filterText: "",
  userHasLoggedIn: false,
  searching: false,
  createTaskIsOpen: false,
}

const reducer = (state = defaultState, action) => {
  switch (action.type) {
    case EDIT_TASK:
      return {
        ...state,
        editingTask: {
          _id: action.task._id,
          title: action.task.title,
          description: action.task.description,
        },
      }

    case LOAD_LIST:
      return {
        ...state,
        tasks: action.tasks,
      }
    case RELOAD_TASK_DATA:
      const tasks = state.tasks.map(t => {
        if (t._id !== action.task._id) {
          return t
        } else {
          t.title = action.task.title
          t.description = action.task.description
          return t
        }
      })
      return {
        ...state,
        tasks,
      }

    case CHANGE_FILTER:
      return {
        ...state,
        filterText: action.text,
        filteredList: state.tasks.filter(t =>
          t.title.toLowerCase().includes(action.text)
        ),
      }

    case USER_HAS_LOGGED_IN:
      return {
        ...state,
        userHasLoggedIn: true,
      }

    case START_SEARCH:
      return {
        ...state,
        searching: true,
      }

    case END_SEARCH:
      return {
        ...state,
        searching: false,
      }

    case OPEN_CREATE:
      return {
        ...state,
        createTaskIsOpen: true,
      }
    case CLOSE_CREATE:
      return {
        ...state,
        createTaskIsOpen: false,
      }

    default:
      return state
  }
}

export default reducer

import {
  EDIT_TASK,
  LOAD_LIST,
  RELOAD_TASK_DATA,
  CHANGE_FILTER,
  USER_HAS_LOGGED_IN,
  START_SEARCH,
  END_SEARCH,
  OPEN_CREATE,
  CLOSE_CREATE,
  UPDATE_CREATE_TASK_DATA,
} from "./type_constants"

export const publishList = tasks => ({
  type: LOAD_LIST,
  tasks,
})

export const setEditingTask = task => {
  return {
    type: EDIT_TASK,
    task,
  }
}

export const reloadTaskData = task => {
  return {
    type: RELOAD_TASK_DATA,
    task,
  }
}

export const changeFilter = text => {
  return {
    type: CHANGE_FILTER,
    text,
  }
}

export const userHasLoggedInAction = () => {
  return {
    type: USER_HAS_LOGGED_IN,
  }
}

export const startSearch = () => {
  return {
    type: START_SEARCH,
  }
}

export const endSearch = () => {
  return {
    type: END_SEARCH,
  }
}

export const openCreate = () => {
  return {
    type: OPEN_CREATE,
  }
}

export const closeCreate = () => {
  return {
    type: CLOSE_CREATE,
  }
}

export const updateCreateTaskData = data => {
  return {
    type: UPDATE_CREATE_TASK_DATA,
    data,
  }
}

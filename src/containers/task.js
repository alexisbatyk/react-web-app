import React from "react"
import { connect } from "react-redux"
import { setEditingTask } from "../state-management/actions"
import TaskComponent from "../components/task-list/task"

const Task = props => {
  const { task, editingTask } = props
  const isBeingUpdated = editingTask._id === task._id

  const getLastValues = () => {
    const taskTitle = document.querySelector(
      ".task-title[contenteditable='true']"
    ).innerText

    return {
      _id: task._id,
      title: taskTitle,
    }
  }

  return (
    <TaskComponent
      {...props}
      isBeingUpdated={isBeingUpdated}
      getLastValues={getLastValues}
    />
  )
}

const mapStateToProps = state => {
  const { editingTask } = state
  return { editingTask: editingTask }
}

const mapDispatchToProps = dispatch => {
  return {
    setEditingTask: task => dispatch(setEditingTask(task)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Task)

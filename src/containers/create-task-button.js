import React from "react"
import { connect } from "react-redux"
import CreateTaskButtonComponent from "../components/create-task/create-task-button"
import { openCreate, closeCreate } from "../state-management/actions"

const CreateTaskButton = props => {
  return <CreateTaskButtonComponent {...props} />
}

const mapStateToProps = state => {
  const { createTaskIsOpen } = state
  return { createTaskIsOpen }
}

const mapDispatchToProps = dispatch => {
  return {
    openCreateAction: () => dispatch(openCreate()),
    closeCreateAction: () => dispatch(closeCreate()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateTaskButton)

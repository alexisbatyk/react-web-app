import React, { useState } from "react"
import { connect } from "react-redux"
import CreateTaskComponent from "../components/create-task/create-task"

const CreateTask = ({ submit, createTaskIsOpen }) => {
  const [title, setTitle] = useState("")
  const [description, setDescription] = useState("")

  const createTask = () => {
    submit(title, description)
    setTitle("")
  }

  if (!createTaskIsOpen) return <></>
  return (
    <CreateTaskComponent
      createTask={createTask}
      title={title}
      setTitle={setTitle}
      description={description}
      setDescription={setDescription}
    />
  )
}

const mapStateToProps = state => {
  const { createTaskIsOpen } = state
  return { createTaskIsOpen }
}

export default connect(mapStateToProps)(CreateTask)

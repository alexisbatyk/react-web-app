import React from "react"
import { connect } from "react-redux"
import {
  startSearch,
  endSearch,
  changeFilter,
} from "../state-management/actions"
import Search from "../components/task-list/search"

const SearchContainer = props => {
  return <Search {...props} />
}

const mapStateToProps = state => {
  const { searching } = state
  return { searching }
}

const mapDispatchToProps = dispatch => {
  return {
    startSearchAction: () => dispatch(startSearch()),
    endSearchAction: () => dispatch(endSearch()),
    changeFilter: text => dispatch(changeFilter(text)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchContainer)

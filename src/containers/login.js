import React, { useState } from "react"
import LoginModal from "../components/login/login-modal"

const LoginContainer = ({ login = () => {}, userHasLoggedIn, showModal }) => {
  const [user, setUser] = useState("")
  const [showError, setShowError] = useState(false)

  if (!showModal) return <></>
  return (
    <LoginModal
      login={login}
      user={user}
      setUser={setUser}
      showError={showError}
      setShowError={setShowError}
      userHasLoggedInAction={userHasLoggedIn}
    />
  )
}

export default LoginContainer

import React from "react"
import styles from "./login.module.css"

const LoginModal = ({
  login = () => {},
  user,
  setUser,
  userHasLoggedInAction,
  showError,
  setShowError,
}) => {
  return (
    <div className={styles.fullScreen}>
      <div className={styles.container}>
        <h3 className={styles.title}>Hi!</h3>

        <input
          className={styles.input}
          placeholder="User"
          onChange={e => setUser(e.target.value)}
          value={user}
        />
        <div className={`${styles.error} ${!showError && styles.hide}`}>
          Login error or user is not correct
        </div>
        <p>Forgot your account?</p>
        <button
          className={styles.submit}
          onClick={async () => {
            setShowError(false)
            const res = await login(user)

            if (res) {
              userHasLoggedInAction()
            } else {
              setUser("")
              setShowError(true)
            }
          }}
        >
          Login
        </button>
      </div>
    </div>
  )
}

export default LoginModal

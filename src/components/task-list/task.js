import React from "react"
import { AiOutlineEdit, AiOutlineDelete, AiOutlineSave } from "react-icons/ai"
import styles from "./task.module.css"

const TaskComponent = ({
  task = { title: "default title", description: "default description" },
  isBeingUpdated = false,
  deleteTask = taskid => {},
  updateTask = () => {},
  setEditingTask = () => {},
  getLastValues = () => {},
}) => (
  <div className={styles.task} key={task._id}>
    <div className={styles.content}>
      <h3
        className={`${styles.taskTitle} task-title`}
        contentEditable={isBeingUpdated}
        suppressContentEditableWarning
      >
        {task.title}
      </h3>
      {/* <p
        className={`${styles.taskDescription} task-description`}
        contentEditable={isBeingUpdated}
        suppressContentEditableWarning
      >
        {task.description}
      </p> */}
    </div>
    <div>
      <button
        className={styles.taskEditButton}
        onClick={() =>
          isBeingUpdated ? updateTask(getLastValues()) : setEditingTask(task)
        }
      >
        {isBeingUpdated ? (
          <div className={"saveIcon"}>
            <AiOutlineSave color={"green"} size={30} />
          </div>
        ) : (
          <div className={"editIcon"}>
            <AiOutlineEdit size={30} />
          </div>
        )}
      </button>

      <button
        className={styles.taskDeleteButton}
        onClick={() => deleteTask(task._id)}
      >
        <AiOutlineDelete size={30} />
      </button>
    </div>
  </div>
)

export default TaskComponent

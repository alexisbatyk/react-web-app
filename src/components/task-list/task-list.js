import React from "react"
import Task from "../../containers/task"
import styles from "./task.module.css"

const TaskList = ({
  tasks = [],
  deleteTask = (id = "") => {},
  updateTask = ({}) => {},
}) =>
  tasks?.length > 0 ? (
    <div className={styles.tasklist}>
      {tasks.map(task => (
        <Task
          key={task._id}
          task={task}
          deleteTask={deleteTask}
          updateTask={updateTask}
        />
      ))}
    </div>
  ) : (
    <div>
      <p>There's no results for your search</p>
    </div>
  )

export default TaskList

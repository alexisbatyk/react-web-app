import React from "react"
import styles from "./task.module.css"
import { AiOutlineSearch, AiOutlineClose } from "react-icons/ai"

const Search = ({
  changeFilter,
  searching,
  startSearchAction,
  endSearchAction,
}) => (
  <div className={styles.searchContainer}>
    {searching && (
      <input
        placeholder="Search..."
        onChange={e => {
          changeFilter(e.target.value)
        }}
        className={styles.inputSearch}
      />
    )}
    {searching ? (
      <AiOutlineClose
        onClick={() => {
          changeFilter("")
          endSearchAction()
        }}
        size={24}
      />
    ) : (
      <AiOutlineSearch onClick={() => startSearchAction()} size={24} />
    )}
  </div>
)

export default Search

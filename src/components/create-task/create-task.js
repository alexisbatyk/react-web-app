import React from "react"
import styles from "./create-task.module.css"
import { AiFillPlusCircle, AiOutlineSend } from "react-icons/ai"

const CreateTaskComponent = ({
  createTask,
  title,
  setTitle,
  description,
  setDescription,
}) => (
  <div className={styles.createTaskContainer}>
    <p className={styles.createOne}>Create one</p>
    <div className={styles.newTask}>
      <input
        className={styles.input}
        type="text"
        name="title"
        placeholder="Title"
        required
        onChange={e => setTitle(e.target.value)}
        value={title}
      ></input>
      {/* <input
      type="text"
      name="description"
      placeholder="Description"
      onChange={e => setDescription(e.target.value)}
      value={description}
    ></input> */}
      <button className={styles.sendNewTask}>
        <AiOutlineSend onClick={() => createTask()} size={26} />
      </button>
    </div>
  </div>
)

export default CreateTaskComponent

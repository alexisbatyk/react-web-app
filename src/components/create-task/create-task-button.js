import React from "react"
import styles from "./create-task.module.css"
import { AiFillPlusCircle, AiOutlineClose } from "react-icons/ai"

const CreateTaskButtonComponent = ({
  createTaskIsOpen,
  openCreateAction,
  closeCreateAction,
}) => (
  <div className={styles.createTaskButton}>
    {createTaskIsOpen ? (
      <AiOutlineClose
        onClick={() => closeCreateAction()}
        size={50}
        color={"green"}
      />
    ) : (
      <AiFillPlusCircle
        onClick={() => openCreateAction()}
        size={50}
        color={"green"}
      />
    )}
  </div>
)

export default CreateTaskButtonComponent

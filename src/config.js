export const API_URL = "http://localhost:4000/api" // should be loaded from env

export const API = { token: "" }

export const API_TOKEN = API.token

export const setToken = token => (API.token = token)

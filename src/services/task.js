import { API_URL, API } from "../config"

export const getList = () => {
  return fetch(`${API_URL}/task`, {
    headers: { Authorization: `Bearer ${API.token}` },
  }).then(res => res.json())
}

export const getTask = id => {
  return fetch(`${API_URL}/task/${id}`).then(res => res.json())
}

export const createTask = (title, description) => {
  return fetch(`${API_URL}/task`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${API.token}`,
    },
    body: JSON.stringify({ title, description }),
  }).then(res => res.json())
}

export const editTask = (id, title, description) => {
  return fetch(`${API_URL}/task/${id}`, {
    method: "PATCH",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${API.token}`,
    },
    body: JSON.stringify({ title, description }),
  }).then(res => res.json())
}

export const deleteTask = id => {
  return fetch(`${API_URL}/task/${id}`, {
    method: "DELETE",
    headers: { Authorization: `Bearer ${API.token}` },
  })
}

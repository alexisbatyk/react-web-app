import { API_URL, setToken } from "../config"

export const login = async (user = "") => {
  try {
    const response = await fetch(`${API_URL}/token`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ user }),
    }).then(res => res.json())
    if (response.error) {
      return false
    }
    if (response.token) {
      setToken(response.token)
      return true
    } else {
      return false
    }
  } catch (e) {
    console.error(e)
    return false
  }
}

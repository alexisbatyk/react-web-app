# README

Task List challenge frontend in React Web

this project should be in the same folder as /api

growth-doctor-challenge/

--- api/

--- react-web-app/

## Start locally

`npm start`

## Testing with Mocha

`npm test`

## Build

`npm run build`

(BIG DISCLAIMER)
The originally idea was to execute `npm lint` `npm test` then `npm run build` in this repo pipeline and then deploy the /build folder to heroku_root/public
but it's not allowed to upload only a specific folder in heroku.
So we have to run `npm run build` then we have to copy the `build/` content to `api/public/` , commit and push all in `api` repo then it will deploy all the project again with the last `/public` folder

## Tech debt

- More tests
- Use another provider for deployment
- Use env for config
